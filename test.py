import urllib
from bs4 import BeautifulSoup
from BeautifulSoup import BeautifulSoup as BS
import re

# 'script', '[document]', 'head', 'title','li','a'

def visible(element):
    if element.parent.name in ['p','li','div','article']:
    	str_format = str(element)
    	if re.search(">", str_format):
    		return False
    	if str_format.count(" ") > 7:
        	return True
    else:
        return False
    


if __name__ == "__main__":
	html = urllib.urlopen('http://job.naij.com/Executive+Administrator-job-85568').read()
	soup = BS(html)
	texts = soup.findAll(text=True)

	visible_texts = filter(visible, texts)

	print visible_texts