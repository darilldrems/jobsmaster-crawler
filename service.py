import urllib2
import urllib
import json
from settings import settings, hdr

from unidecode import unidecode

def download_landing_pages(country):
	url = settings["endpoints"]["host"] + settings["endpoints"]["api_landing_pages"]

	if country:
		url += "?country="+country

	response = urllib2.urlopen(url)

	page = response.read()
	landing_pages = json.loads(page)

	response.close()
	
	return landing_pages

def download_group_landing_pages(country, size):
	url = settings["endpoints"]["host"] + settings["endpoints"]["api_group_landing_pages"]

	if country:
		url += "?country="+country+"&total="+str(size)

	response = urllib2.urlopen(url)

	page = response.read()
	landing_pages = json.loads(page)

	response.close()
	
	return landing_pages

def add_crawled_url(page):
	data = {}
	data["url"] = page

	url_values = urllib.urlencode(data)

	url = settings["endpoints"]["host"] + settings["endpoints"]["api_add_crawled_url"] + "?"+url_values

	response = urllib2.urlopen(url)

	response.close()

	pass

def download_job_pages(country):
	url = settings["endpoints"]["host"] + settings["endpoints"]["api_job_pages"]

	if country:
		url += "?country="+country

	response = urllib2.urlopen(url)

	page = response.read()

	response.close()

	pages = json.loads(page)
	return pages

def download_group_job_pages(country, size):
	url = settings["endpoints"]["host"] + settings["endpoints"]["api_group_job_pages"]

	if country:
		url += "?country="+country+"&total="+str(size)

	response = urllib2.urlopen(url)

	page = response.read()



	response.close()

	pages = json.loads(page)
	# print pages
	return pages

def download_home_pages(country):
	url = settings["endpoints"]["host"] + settings["endpoints"]["api_home_pages"]

	if country:
		url += "?country="+country

	response = urllib2.urlopen(url)

	page = response.read()

	response.close()

	return json.loads(page)

def add_url(link, site):
	url = settings["endpoints"]["host"] + settings["endpoints"]["api_add_url"]

	data ={}

	data["url"] = link
	data["site"]= site

	url_values = urllib.urlencode(data)

	url = url + "?" + url_values

	# print url_values

	response = urllib2.urlopen(url)

	response.close()

	pass

def add_job(**kwargs):
	url = settings["endpoints"]["host"]+settings["endpoints"]["api_submit_job"]

	data = {}

	if kwargs.get("title"):
		data["title"] = unidecode(kwargs.get("title"))

	if kwargs.get("summary"):
		data["summary"] = unidecode(kwargs.get("summary"))

	if kwargs.get("content"):
		data["content"] = unidecode(kwargs.get("content").encode("ascii", "ignore"))

	if kwargs.get("minimum_salary"):
		data["minimum_salary"] = unidecode(kwargs.get("minimum_salary"))

	if kwargs.get("maximum_salary"):
		data["maximum_salary"] = unidecode(kwargs.get("maximum_salary"))

	if kwargs.get("location"):
		data["location"] = unidecode(kwargs.get("location"))

	if kwargs.get("secondary_location"):
		data["secondary_location"] = unidecode(kwargs.get("secondary_location"))

	if kwargs.get("country"):
		data["country"] = unidecode(kwargs.get("country"))

	if kwargs.get("experience"):
		data["experience"] = unidecode(kwargs.get("experience"))

	if kwargs.get("company"):
		data["company"] = unidecode(kwargs.get("company"))

	if kwargs.get("job_type"):
		data["job_type"] = unidecode(kwargs.get("job_type"))

	if kwargs.get("words"):
		data["words"] = unidecode(kwargs.get("words"))

	if kwargs.get("site"):
		data["site"] = kwargs.get("site")

	if kwargs.get("url"):
		data["url"] = kwargs.get("url")

	try:
		response = urllib2.urlopen(url, urllib.urlencode(data))
		response.close()
	except urllib2.HTTPError, e:
		print e.read()
	
		

if __name__ == "__main__":
	# print download_landing_pages()
	data = {
		"country": "Nigeria",
		"site": "Jobberman",
		"title": "test job",
		"summary": "I am testing this job out",
		"content": "what are you saying boy",
		"url": "http://jobberman.com/mrman"
	}

	add_job(**data)