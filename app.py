# coding: utf8
import service
import utilities

from forbiden_words import words

import sys
import multiprocessing


def crawl_landing_pages(country):
	landing_pages = service.download_landing_pages(country)

	# print landing_pages

	crawl_pages(landing_pages)

#this process uses multiprocessing to launch concurrent work on various website crawling
def crawl_group_landing_pages(country):
	group_landing_pages = service.download_group_landing_pages(country, 100)

	print len(group_landing_pages)

	pool = multiprocessing.Pool(10)

	results = []
	for landing_pages in group_landing_pages:
		results.append(pool.apply_async(crawl_pages, args=(landing_pages,)))

	for p in results:
		p.get()

	

	# [pool.apply(crawl_pages, args=(landing_pages)) for landing_pages in group_landing_pages]
	

	# print landing_pages

	# crawl_pages(landing_pages)

def crawl_group_job_pages(country):
	group_job_pages = service.download_group_job_pages(country, 100)

	pool = multiprocessing.Pool(10)
	results = []
	for job_pages in group_job_pages:
		# print job_pages
		results.append(pool.apply_async(crawl_job_pages, args=(job_pages,)))

	for p in results:
		p.get()


def crawl_pages(landing_pages):
	for page in landing_pages:
		try:
		
			links = utilities.load_website_and_get_links(page['url'])

			# print links

			absolute_path_urls = utilities.fix_urls(links, page['url'])
			
			# filter urls not in the same domain here
			absolute_path_urls = utilities.filter_other_sites_from_urls(page['url'], absolute_path_urls)
			
			print "about to add url"
			
			for absolute_url in absolute_path_urls:
				service.add_url(absolute_url, page['site']['name'])
		except:
			pass

		

	pass

def crawl_page(page):
	try:
		
		links = utilities.load_website_and_get_links(page['url'])

		# print links

		absolute_path_urls = utilities.fix_urls(links, page['url'])
		
		# filter urls not in the same domain here
		absolute_path_urls = utilities.filter_other_sites_from_urls(page['url'], absolute_path_urls)
		
		print "about to add url"
		
		for absolute_url in absolute_path_urls:
			service.add_url(absolute_url, page['site']['name'])
	except:
		pass


def crawl_job_pages(job_pages):
	# job_pages = service.download_job_pages(country)

	for job_page in job_pages:
		
		url = job_page.get("url")
		print url

		if "google.com" in url:
			continue

		if "facebook.com" in url:
			continue

		if "twitter.com" in url:
			continue

		# print url
		if url not in words:
			crawl_pages(url)
			status, job_page_html = utilities.load_website(url)

			print "loaded the page"
			print job_page_html
			# print 
			# job_page_html = job_page_response.read()

			# print "website status is : "+status

			if status:
				# print "parsing the page for job"
				site = job_page.get("site")
				status, job = utilities.parse_job_page(job_page_html, site)

				print "parse job stage"
				print job
				print status

				if status:
					job["url"] = url
					job["country"] = job_page["country"]["name"]
					job["site"] = site.get("name")

					print job
					service.add_job(**job)

		
		service.add_crawled_url(url)
		



	pass

def crawl_home_pages(country):
	pages = service.download_home_pages(country)

	results = []
	pool = multiprocessing.Pool(3)
	for page in pages:
		results.append(pool.apply_async(crawl_page, args=(page,)))

	for p in results:
		p.get()

	pass


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "ERROR: No task passed"
	else:
		task = sys.argv[1]
		country = sys.argv[2]

		print task

		if task == "jobs_pages":
			crawl_group_job_pages(country)
		
		if task == "landing_pages":
			print "there"
			crawl_group_landing_pages(country)

		if task == "home":
			crawl_home_pages(country)

		
