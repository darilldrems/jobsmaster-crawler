import service
import utilities

from forbiden_words import words

import sys

def crawl_landing_pages(country):
	group_landing_pages = service.download_group_landing_pages(country)

	# print landing_pages

	crawl_pages(landing_pages)

def crawl_pages(landing_pages):
	for page in landing_pages:
		try:
		
			links = utilities.load_website_and_get_links(page['url'])

			# print links

			absolute_path_urls = utilities.fix_urls(links, page['url'])
			
			# filter urls not in the same domain here
			absolute_path_urls = utilities.filter_other_sites_from_urls(page['url'], absolute_path_urls)
			
			
			for absolute_url in absolute_path_urls:
				service.add_url(absolute_url, page['site']['name'])
		except:
			pass