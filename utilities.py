from urlparse import urljoin, urlparse
from bs4 import BeautifulSoup
from BeautifulSoup import BeautifulSoup as BS
import re
import urllib2
import urllib

from settings import hdr

import nltk

from lxml import html, etree
import lxml
import lxml.html.soupparser

from io import StringIO, BytesIO

from unidecode import unidecode

#this will turn relative path to absolute path
def fix_url(href, base_url):
	return urljoin(base_url, href)
	pass

def fix_urls(hrefs, base_url):
	fixed_urls =[]

	for href in hrefs:
		url = fix_url(href, base_url)
		fixed_urls.append(url)

	return fixed_urls

#function to check if link has bad words
def has_no_bad_words(data):
	for word in forbiden_words:
		if word in data:
			return False
	return True
	
def filter_other_sites_from_urls(link, urls):
	hostname = urlparse(link).hostname
	return [url for url in urls if hostname in url]

def valid_xml_char_ordinal(c):
    codepoint = ord(c)
    # conditions ordered by presumed frequency
    return (
        0x20 <= codepoint <= 0xD7FF or
        codepoint in (0x9, 0xA, 0xD) or
        0xE000 <= codepoint <= 0xFFFD or
        0x10000 <= codepoint <= 0x10FFFF
        )

def load_website(url):
	req = urllib2.Request(url, headers=hdr)
	# req = urllib2.Request(url, headers=hdr)

	try:
		response = urllib2.urlopen(req)

		page = response.read()

		cleaned_page = ''.join(c for c in page if valid_xml_char_ordinal(c))
	except Exception as e:
		print e
		return (False, "")

	# if with_response:
	# 	return (True, response)

	return (True, cleaned_page)




def load_website_and_get_links(url):
	links = []
	req = urllib2.Request(url, headers=hdr)

	try:
		response = urllib2.urlopen(req)
	except:
		return links

	body = response.read()

	doc = BeautifulSoup(body, "lxml")



	for link in doc.findAll('a'):
		href = link.get('href')
		if href:
			if href.lower() != "":
				if href not in links:
					links.append(fix_url(href, url))

	return links


def __get_string_from_element(element):
	st = ""

	if isinstance(element, lxml.etree._ElementStringResult):
		st = str(element)
		# print st
				
	if isinstance(element, lxml.html.HtmlElement):
		st = element.text
		# print st

	return st

# this method is used to retrive all text in a webpage
def visible(element):
    if element.parent.name in ['p','li','div','article', 'span']:
    	str_format = str(element)
    	if re.search(">", str_format):
    		return False
    	if str_format.count(" ") > 7:
        	return True
    else:
        return False

def parse_job_page(page, xpath):
	summary_xpath = '//meta[@name="description"]/@content'
	job = {}


	if xpath.get("title_xpath"):

		# parser = etree.HTMLParser()

		# if response:
		# 	tree   = etree.parse(response, parser)
		# else:
		# 	tree = etree.parse(StringIO(unidecode(page)), parser)
		
		# fixed_html = etree.tostring(tree.getroot(), method="html")

		tree = lxml.html.soupparser.fromstring(page)

		meta_description = tree.xpath(summary_xpath)

		# print summary_xpath
		# print meta_description

		if len(meta_description) > 0:
			job["summary"] = meta_description[0]
		
		title_xpath = xpath.get("title_xpath")
		# print title_xpath

		if title_xpath:
			title = tree.xpath(title_xpath)

			print title_xpath
			print title

			if len(title) > 0:
				first = title[0]

				# print first

				job["title"] = __get_string_from_element(first)
				job["words"] = ",".join(nltk.word_tokenize(job["title"]))

				print job["title"]

				if not job["title"].strip("\n"):
					print "in the 1st"
					titles = tree.xpath("/h1")
					if len(titles) > 0:
						job["title"] = __get_string_from_element(titles[0])

			else:
				print "in the 2nd"
				titles = tree.xpath("//h1")
				if len(titles) > 0:
					job["title"] = __get_string_from_element(titles[0])


				


		# content_xpath = xpath.get("description_xpath")
		# if content_xpath:
		# 	content = tree.xpath(content_xpath)

		# 	if len(content) > 0:
		# 		first = content[0]

		# 		job["content"] = __get_string_from_element(first)

		soup = BS(page)
		texts = soup.findAll(text=True)

		visible_texts = filter(visible, texts)
		# print visible_texts
		# print "after visivle texts"
		job["content"] = " ".join(visible_texts)
		# print job["content"]



		job_type_xpath = xpath.get("job_type_xpath")
		if job_type_xpath:
			job_type = tree.xpath(job_type_xpath)

			if len(job_type) > 0:
				first = job_type[0]

				job["job_type"] = __get_string_from_element(first)


		salary_xpath = xpath.get("salary_xpath")
		if salary_xpath:
			salary = tree.xpath(salary_xpath)

			if len(salary) > 0:
				first = salary[0]

				job["minimum_salary"] = __get_string_from_element(first)


		# qualification_xpath = xpath.get("qualification_xpath")
		# if qualification_xpath:
		# 	qualification = tree.xpath(qualification_xpath)

		# 	if len(qualification) > 0:
		# 		first = qualification[0]

		# 		job["qualification"] = __get_string_from_element(first)


		location_xpath = xpath.get("location_xpath")
		if location_xpath:
			location = tree.xpath(location_xpath)

			if len(location) > 0:
				first = location[0]

				job["location"] = __get_string_from_element(first)


		experience_xpath = xpath.get("experience_xpath")
		if experience_xpath:
			experience = tree.xpath(experience_xpath)

			if len(experience) > 0:
				first = experience[0]

				job["experience"] = __get_string_from_element(first)


		company_xpath = xpath.get("company_xpath")
		if company_xpath:
			company = tree.xpath(company_xpath)

			if len(company) > 0:
				first = company[0]

				job["company"] = __get_string_from_element(first)

		if job.get("title") and job.get("summary"):
			return (True, job)
			
		print job

	return (False, {})
	
if __name__ == "__main__":
	# page = urllib2.urlopen("http://www.jobberman.com/job/429374/customer-relation-officer-mutual-benefits-assurance-plc-in-lagos").read()
	
	# print "in here"
	# parse_job_page(page, {"title_xpath": "//*[@id='new-home-page']/div[5]/h1"})
	
	urls = ["http://google.com", "http://jobsmaster.com.ng/aler", "http://jobsmaster.com.ng/search"]
	
	print filter_other_sites_from_urls("http://jobsmaster.com.ng/categories", urls)
	
	
